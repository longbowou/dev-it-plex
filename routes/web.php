<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::view('/', 'welcome');

Route::prefix('user')->namespace('User')->name('user.')->group(function () {
    Auth::routes();

    Route::get('/home', 'UserController@index')->name('home');
});

Route::prefix('admin')->namespace('Admin')->name('admin.')->group(function () {
    Auth::routes();

    Route::get('/home', 'AdminController@index')->name('home');
});
