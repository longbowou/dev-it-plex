<?php

namespace App\Http\Controllers\User\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->redirectTo = route("user.home");

        $this->middleware('guest:' . User::GUARD)->except('logout');
    }

    public function showLoginForm()
    {
        return view('user.auth.login');
    }

    public function username()
    {
        return 'username';
    }

    protected function credentials(Request $request)
    {
        $requestData = ['password' => $request->input('password')];
        validator($request->all(), ["username" => "email"])->fails() ? ($requestData["username"] = $request->username) : ($requestData["email"] = $request->username);
        return $requestData;
    }

    protected function guard()
    {
        return Auth::guard(User::GUARD);
    }
}
